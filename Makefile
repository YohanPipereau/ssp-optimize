CC=/opt/wasi-sdk/bin/clang
CFLAGS=-I  /opt/wasi-sdk/share/wasi-sysroot/include
LFLAGS=-L /opt/wasi-sdk/share/wasi-sysroot/lib/wasm32-wasi/ -L/opt/wasi-sdk/lib/clang/16/lib/wasi -lclang_rt.builtins-wasm32 -lc -Wl,--stack-first

WASI_SDK=/home/yohan/quentin/wasi-sdk
CUSTOM_LLVM=${WASI_SDK}/build/llvm/
CUSTOM_LLVM_BIN=$(CUSTOM_LLVM)/bin
WASI_RT=$(WASI_SDK)/build/compiler-rt/lib/wasi

QCC=${WASI_SDK}/build/install/opt/wasi-sdk/bin/clang
QCFLAGS=-I ${WASI_SDK}/build/install/opt/wasi-sdk/share/wasi-sysroot/include/wasm32-wasi
QLFLAGS=-L ${WASI_RT} \
	-L ${WASI_SDK}/build/install/opt/wasi-sdk/share/wasi-sysroot/lib/wasm32-wasi/ \
	-lclang_rt.builtins-wasm32 -lc \
	-fstack-protector-all\
	-Wl,--stack-first

all: opt0 opt2 nativeO2 nativeO0


opt0:
	$(QCC) --target=wasm32-unknown-wasi -o opt0.wasm main.c -O0 $(QCFLAGS) $(QLFLAGS)

opt2:
	$(QCC) --target=wasm32-unknown-wasi -o opt2.wasm main.c -O2 $(QCFLAGS) $(QLFLAGS)

normal:
	$(CC) --target=wasm32-unknown-wasi -o normal.wasm main.c $(CFLAGS) $(LFLAGS)

nativeO2:
	clang -o nativeO2 main.c -O2 -fstack-protector-all

nativeO0:
	clang -o nativeO0 main.c -O0 -fstack-protector-all




#Why does -O0 crash and not -O1
quentin:
	for flag in `cat flags.txt`; do \
		echo $$flag ;\
		$(QCC) --target=wasm32-unknown-wasi $(QCFLAGS) $(QLFLAGS) $$flag -o quentin_$$flag.wasm main.c  ;\
	done

memory-layout:
	$(CC) --target=wasm32-unknown-wasi -o memlayout.wasm memlayout.c $(CFLAGS) $(LFLAGS)

clean:
	rm *.wasm nativeO0 nativeO2
